#!/usr/bin/env python2

import json,urllib
import pymongo
from pymongo import MongoClient

def open_roa_file(url):
        response = urllib.urlopen(url)
        data = json.loads(response.read())
        return data

def get_list_of_items(data):
        list_of_items = []
        for key, value in data.items():
                for items in value:
                        if '::' in items['prefix']:
                                continue
                        else:
                                list_of_items.append(items)
        return list_of_items

def OpenMongo():
    try:
        conn = MongoClient()
        print("Connected successfully!!!")
        return conn
    except:
        print("Could not connect to MongoDB")
        exit

def CreateROACol(conn):
    db = conn.ROAS
    roa_collection = db.roas
    return roa_collection

def getFilelist(file):
    with open("/home/moyaze/all_routes.txt") as f:
        lineList = f.readlines()
    return lineList

def populateROA(roa_collection):
    x = roa_collection.delete_many({})
    for item in list_of_items:
        roa = item
        try:
            rec = roa_collection.insert_one(roa)
            #print "inserted roa - "+item['prefix']
        except pymongo.errors.DuplicateKeyError:
            #print "Duplicate ROA found for "+str(roa)
            continue
    return

def getBGPattributes(lineList):
    for line in lineList:
        if 'bird' in line or 'BIRD' in line:
            continue
        else:
            line_split = (line.split())

            if len(line.split()) == 11:
                prefix = str(line_split[0])
                prefix_list.append(prefix)
                next_hop = str(line_split[2])
                next_hop_list.append(next_hop)
                next_hop_as = str(line_split[5].lstrip('[R'))
                next_hop_as=next_hop_as[:-2]
                next_hop_as_list.append(next_hop_as)
                origin_as = str(line_split[-1].lstrip('[AS'))
                origin_as = origin_as[:-2]
                origin_as_list.append(origin_as)

            elif len(line.split()) == 9:
                prefix = str(line_split[0])
                prefix_list.append(prefix)
                next_hop = str(line_split[1])
                next_hop_list.append(next_hop)
                next_hop_as = line_split[4].lstrip('[R')
                next_hop_as=str(next_hop_as[:-2])
                next_hop_as_list.append(next_hop_as)
                origin_as = str(line_split[-1].lstrip('[AS'))
                origin_as = origin_as[:-2]
                origin_as_list.append(origin_as)

            else:
                print "not processsed"
                print line_split
    return (prefix_list,next_hop_list,next_hop_as_list,origin_as_list)

def createPrefixesCol(data, collection):
    x = collection.delete_many({})
    for elements in data:
        network = elements[0]
        record = "{'prefix':'"+network+", origin_as: "+elements[3]+", next_hop: "+elements[1]+", next_hop_as: "+elements[2]+", rpki_state: 0, reason: not_known'}"
        dict_record = eval(record)

        try:
            rec = collection.insert_one(dict_record)
            #print "inserted "+elements[0]
        except pymongo.errors.DuplicateKeyError:
            continue 
    return collection


####MAIN CONFIG#######

prefix_list=[]
next_hop_list=[]
next_hop_as_list=[]
origin_as_list=[]
#This part create the VRP LIST from ROA Table. It grabs the data from the NTT RPKI validator...(cheeky to use)
url = "https://rpki.gin.ntt.net/api/export.json"
data =open_roa_file(url)

#print data
list_of_items = get_list_of_items(data)
#print list_of_items

conn = OpenMongo()

#    print("Could not connect to MongoDB")
roa_collection = CreateROACol(conn)
populateROA(roa_collection)

print "ROA's loaded into MongoDB"

#get routes list
file = "all_routes.txt"
lineList = getFilelist(file)
#Create lists for each BGP attribute needed for RPKI
(prefix_list,next_hop_list,next_hop_as_list,origin_as_list) = getBGPattributes(lineList)

count =0
for element in prefix_list:
    if 'via' not in element:
        count +=1
    else:
        previous_count = count-1
        prefix_list[count]=prefix_list[previous_count]
        count +=1

if len(prefix_list)==len(next_hop_list)==len(next_hop_as_list)==len(origin_as_list):
    print "Data for all varibles are correct"
try:
    conn = MongoClient()
    print("Connected successfully!!!")
except:
    print("Could not connect to MongoDB")

# database name: mydatabase 
db = conn.PREFIXES

# Created or Switched to collection names: myTable
collection = db.prefixes
data = zip(prefix_list,next_hop_list,next_hop_as_list,origin_as_list)
createPrefixesCol(data,collection)

'''
cursor_roas = roa_collection.find()
cursor_prefixes =collection.find()

for document in cursor_prefixes:
        #document_split = (document['prefix']).split(',')
        #for record in cursor_roas:
         #   print (record['prefix'])+" "+document_split[0]
    for k, v in document.iteritems():
        #print v.split(',')
        print type(v)

'''
            



print "the end"
